# Modulo7Calculator #

**The easiest way to run is to download [Modulo7Calculator APKs](https://bitbucket.org/caiiiyua/modulo7calculator/downloads/modulo7calculator.tar.gz), install it directly on your device/simulator.**

Or

You could download source to run from scratch.
[Modulo7Calculator Full Source](https://bitbucket.org/caiiiyua/modulo7calculator/get/57b5097e34bd.zip)

### What Modulo7Calculator is ? ###

** It is a modulo 7 calculator practice demo. **

* The Modulo7Calculator provide the following functionality:
```
1. Adding two numbers modulo 7
2. Subtracting two numbers modulo 7
3. Multiplying two numbers modulo 7
```
* Technical specifications:
```
1. Compatibility with Android 5.0 and later version. 
2. Work well in both landscape and portrait orientation
3. The UX part is implemented in Java
4. The calculation part is implemented in C(libmycalculator.so)
```
![screenshot.png](https://bitbucket.org/repo/ed7Gdq/images/3598354778-screenshot.png)

* Class diagram.
![Calculator.png](https://bitbucket.org/repo/ed7Gdq/images/618070758-Calculator.png)


### Testcase report ###
![屏幕快照 2016-03-13 下午2.00.57.png](https://bitbucket.org/repo/ed7Gdq/images/3945536104-%E5%B1%8F%E5%B9%95%E5%BF%AB%E7%85%A7%202016-03-13%20%E4%B8%8B%E5%8D%882.00.57.png)
![屏幕快照 2016-03-13 下午2.01.09.png](https://bitbucket.org/repo/ed7Gdq/images/2658123481-%E5%B1%8F%E5%B9%95%E5%BF%AB%E7%85%A7%202016-03-13%20%E4%B8%8B%E5%8D%882.01.09.png)
![屏幕快照 2016-03-13 下午2.01.27.png](https://bitbucket.org/repo/ed7Gdq/images/1817735598-%E5%B1%8F%E5%B9%95%E5%BF%AB%E7%85%A7%202016-03-13%20%E4%B8%8B%E5%8D%882.01.27.png)

### How do I get set up? ###

*  Git clone the source code
```
#!shell

git clone https://caiiiyua@bitbucket.org/caiiiyua/modulo7calculator.git
./gradlew build
```

* How to run tests
```
#!shell
./gredlew cAT
```

* Dependencies
```
#!gradle
:app:androidDependencies
debug
\--- com.android.support:appcompat-v7:23.0.1
     \--- com.android.support:support-v4:23.0.1
          \--- LOCAL: internal_impl-23.0.1.jar

debugAndroidTest
+--- com.android.support.test:runner:0.4.1
|    \--- com.android.support.test:exposed-instrumentation-api-publish:0.4.1
+--- com.android.support.test:rules:0.4.1
|    \--- com.android.support.test:runner:0.4.1
|         \--- com.android.support.test:exposed-instrumentation-api-publish:0.4.1
+--- com.android.support.test.uiautomator:uiautomator-v18:2.1.2
\--- com.android.support.test.espresso:espresso-core:2.2.1
     +--- com.android.support.test:runner:0.4.1
     |    \--- com.android.support.test:exposed-instrumentation-api-publish:0.4.1
     +--- com.android.support.test:rules:0.4.1
     |    \--- com.android.support.test:runner:0.4.1
     |         \--- com.android.support.test:exposed-instrumentation-api-publish:0.4.1
     \--- com.android.support.test.espresso:espresso-idling-resource:2.2.1

debugUnitTest
No dependencies

release
\--- com.android.support:appcompat-v7:23.0.1
     \--- com.android.support:support-v4:23.0.1
          \--- LOCAL: internal_impl-23.0.1.jar

releaseUnitTest
No dependencies
```