package com.example.pp.modulo7calculator;

import android.support.test.runner.AndroidJUnit4;
import android.test.suitebuilder.annotation.SmallTest;

import com.example.pp.modulo7calculator.calculator.AddCalculator;
import com.example.pp.modulo7calculator.calculator.MinusCalculator;
import com.example.pp.modulo7calculator.calculator.MultiplyCalculator;

import org.junit.Test;
import org.junit.runner.RunWith;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

/**
 * Created by pp on 16/3/13.
 */
@RunWith(AndroidJUnit4.class)
@SmallTest
public class CalculatorTest {

    @Test
    public void testCalculate() throws Exception {
        System.out.printf("testcase started");
        assertThat(new AddCalculator().compute(2, 3), is((2 + 3) % 7));
        assertThat(new MultiplyCalculator().compute(2, 3), is((2 * 3) % 7));
        assertThat(new MinusCalculator().compute(2, 3), is((2 - 3) % 7));
        assertThat(new MultiplyCalculator().compute(1024, 1024), is((1024 + 1024) % 7));
        assertThat(new MinusCalculator().compute(2, 2048), is((2 - 2048) % 7));

//        assertEquals((2 + 3) % 7, new AddCalculator().compute(2, 3));
//        assertEquals((2 * 3) % 7, new MultiplyCalculator().compute(2, 3));
//        assertEquals((2 - 3) % 7, new MinusCalculator().compute(2, 3));
//        assertEquals((1024 * 1024) % 7, new MultiplyCalculator().compute(1024, 1024));
//        assertEquals((2 - 2048) % 7, new MinusCalculator().compute(2, 2048));
    }
}