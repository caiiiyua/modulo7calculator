package com.example.pp.modulo7calculator;

import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.test.suitebuilder.annotation.SmallTest;

import com.example.pp.modulo7calculator.calculator.AddCalculator;
import com.example.pp.modulo7calculator.calculator.MinusCalculator;
import com.example.pp.modulo7calculator.calculator.MultiplyCalculator;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;

/**
 * Created by pp on 16/3/13.
 */
@RunWith(AndroidJUnit4.class)
@SmallTest
public class Modulo7CalculatorTestCase {

    @Before
    public void setUp() throws Exception {
    }

    @Rule
    public ActivityTestRule<CalculatorActivity> mActivityRule = new ActivityTestRule<>(
            CalculatorActivity.class);

    @Test
    public void testMultiply() {
        // 2 * 6 = 5
        onView(withId(R.id.two)).perform(click());
        onView(withId(R.id.times)).perform(click());
        onView(withId(R.id.six)).perform(click());
        onView(withId(R.id.equalto)).perform(click());
        onView(withId(R.id.result))
                .check(matches(withText(
                        String.valueOf((2 * 6) % 7))));
    }

    @Test
    public void testMinus() {
        // 2 - 16 = 0
        onView(withId(R.id.two)).perform(click());
        onView(withId(R.id.minus)).perform(click());
        onView(withId(R.id.one)).perform(click());
        onView(withId(R.id.six)).perform(click());
        onView(withId(R.id.equalto)).perform(click());
        onView(withId(R.id.result))
                .check(matches(withText(
                        String.valueOf((2 - 16) % 7))));
    }

    @Test
    public void testAdd() {
        // 2 + 6 = 1
        onView(withId(R.id.two)).perform(click());
        onView(withId(R.id.add)).perform(click());
        onView(withId(R.id.six)).perform(click());
        onView(withId(R.id.equalto)).perform(click());
        onView(withId(R.id.result))
                .check(matches(withText(
                        String.valueOf((2 + 6) % 7))));
    }

    @Test
    public void testMultipleCalculate() {
        // Continuous performing (2+6)*5-10
        int r = new AddCalculator().compute(2 ,6);
        r = new MultiplyCalculator().compute(r, 5);
        r = new MinusCalculator().compute(r, 10);

        onView(withId(R.id.two)).perform(click());
        onView(withId(R.id.add)).perform(click());
        onView(withId(R.id.six)).perform(click());

        onView(withId(R.id.times)).perform(click());
        onView(withId(R.id.five)).perform(click());

        onView(withId(R.id.minus)).perform(click());
        onView(withId(R.id.one)).perform(click());
        onView(withId(R.id.zero)).perform(click());

        onView(withId(R.id.equalto)).perform(click());
        onView(withId(R.id.result))
                .check(matches(withText(
                        String.valueOf(r))));
    }

    @Test
    public void testDelete() {
        // 1234 -> delete(4) -> 123560 -> delete(0) -> 12356
        onView(withId(R.id.one)).perform(click());
        onView(withId(R.id.two)).perform(click());
        onView(withId(R.id.three)).perform(click());
        onView(withId(R.id.four)).perform(click());

        onView(withId(R.id.del)).perform(click());

        onView(withId(R.id.five)).perform(click());
        onView(withId(R.id.six)).perform(click());
        onView(withId(R.id.zero)).perform(click());

        onView(withId(R.id.del)).perform(click());
        onView(withId(R.id.result))
                .check(matches(withText("12356")));
    }

    @Test
    public void testDeleteResult() {
        // 14 + 6 -> delete -> empty
        onView(withId(R.id.one)).perform(click());
        onView(withId(R.id.four)).perform(click());

        onView(withId(R.id.add)).perform(click());
        onView(withId(R.id.six)).perform(click());

        onView(withId(R.id.equalto)).perform(click());
        onView(withId(R.id.del)).perform(click());
        onView(withId(R.id.result))
                .check(matches(withText("")));
    }

    @Test
    public void testLargeNumbers() {
        // 66666666 * 66666666 = 2
        long ln = 66666666;
        int r = (int)((ln * ln) % 7);
        onView(withId(R.id.six)).perform(click());
        onView(withId(R.id.six)).perform(click());
        onView(withId(R.id.six)).perform(click());
        onView(withId(R.id.six)).perform(click());
        onView(withId(R.id.six)).perform(click());
        onView(withId(R.id.six)).perform(click());
        onView(withId(R.id.six)).perform(click());
        onView(withId(R.id.six)).perform(click());

        onView(withId(R.id.times)).perform(click());
        onView(withId(R.id.six)).perform(click());
        onView(withId(R.id.six)).perform(click());
        onView(withId(R.id.six)).perform(click());
        onView(withId(R.id.six)).perform(click());
        onView(withId(R.id.six)).perform(click());
        onView(withId(R.id.six)).perform(click());
        onView(withId(R.id.six)).perform(click());
        onView(withId(R.id.six)).perform(click());

        onView(withId(R.id.equalto)).perform(click());
        onView(withId(R.id.result))
                .check(matches(withText(String.valueOf(r))));
    }
}
