package com.example.pp.modulo7calculator.adapter;

/**
 * Created by pp on 16/3/12.
 */
public class ComputingAdapter {
    /**
     * JNI for computing a and b
     * @param a a number in long
     * @param b a number in long
     * @param cmd an action to be performed
     * @return int as result of performing on a and b
     */
    public native int compute(long a, long b, int cmd);

    static {
        System.loadLibrary("mycalculator");
    }
}
