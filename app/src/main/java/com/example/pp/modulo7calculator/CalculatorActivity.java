package com.example.pp.modulo7calculator;

import android.app.Activity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.example.pp.modulo7calculator.calculator.AddCalculator;
import com.example.pp.modulo7calculator.calculator.Calculator;
import com.example.pp.modulo7calculator.calculator.ICalculator;
import com.example.pp.modulo7calculator.calculator.MinusCalculator;
import com.example.pp.modulo7calculator.calculator.MultiplyCalculator;

public class CalculatorActivity extends Activity implements View.OnClickListener {

    private static final int ADD = 1;
    private static final int MINUS = ADD << 1;
    private static final int MUL = ADD << 2;
    private static final int ACTION_INPUT = 100;
    private static final int ACTION_SET_OPERATOR = ACTION_INPUT + 1;
    private static final int ACTION_CALCULATE = ACTION_INPUT + 2;
    private static final String TAG = "MYCALCULATOR";
    private static final int MAX_DIGITS = 9;

    private TextView mResultView;
    private CalculatorState mState;
    private long mNumber1 = 0;
    private long mNumber2 = 0;

    private int mEval;
    private int mAction;
    private int mLastResult;

    private enum CalculatorState {
        INIT, FIRST_NUMBER, OPERATOR, SECOND_NUMBER, RESULT
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mResultView = (TextView) findViewById(R.id.result);
        Button one = (Button) findViewById(R.id.one);
        Button two = (Button) findViewById(R.id.two);
        Button three = (Button) findViewById(R.id.three);
        Button four = (Button) findViewById(R.id.four);
        Button five = (Button) findViewById(R.id.five);
        Button six = (Button) findViewById(R.id.six);
        Button add = (Button) findViewById(R.id.add);
        Button minus = (Button) findViewById(R.id.minus);
        Button equalto = (Button) findViewById(R.id.equalto);
        Button del = (Button) findViewById(R.id.del);
        Button times = (Button) findViewById(R.id.times);
        Button zero = (Button) findViewById(R.id.zero);
        one.setOnClickListener(this);
        two.setOnClickListener(this);
        three.setOnClickListener(this);
        four.setOnClickListener(this);
        five.setOnClickListener(this);
        six.setOnClickListener(this);
        zero.setOnClickListener(this);
        add.setOnClickListener(this);
        minus.setOnClickListener(this);
        equalto.setOnClickListener(this);
        del.setOnClickListener(this);
        times.setOnClickListener(this);

        mResultView.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                onEvaluate(mAction, s != null ? s.toString() :  "");
            }
        });

        mState = CalculatorState.FIRST_NUMBER;

        if (savedInstanceState != null) {
            long previous = savedInstanceState.getLong("RESULT_VALUE");
            mResultView.setText(String.valueOf(previous));
            mEval = savedInstanceState.getInt("EVAL");
        }
    }

    private void onEvaluate(int action, String text) {
        switch (mState) {
            case INIT:
                break;
            case FIRST_NUMBER:
                if (action == ACTION_SET_OPERATOR) {
                    mState = CalculatorState.OPERATOR;
                    break;
                } else if (action == ACTION_CALCULATE) {
                    mState = CalculatorState.RESULT;
                }
                if (text.length() > 0) {
                    mNumber1 = Long.valueOf(text);
                } else {
                    mNumber1 = 0;
                }

                break;
            case OPERATOR:
                if (action == ACTION_INPUT) {
                    mState = CalculatorState.SECOND_NUMBER;
                    if (text.length() > 0) {
                        mNumber2 = Long.valueOf(text);
                    } else {
                        mNumber2 = 0;
                    }
                }
                break;
            case SECOND_NUMBER:
                if (action != ACTION_INPUT) {
                    mState = CalculatorState.RESULT;
                    mNumber1 = Long.valueOf(text);
                    mNumber2 = 0;
                    break;
                }
                if (text.length() > 0) {
                    mNumber2 = Long.valueOf(text);
                } else {
                    mNumber2 = 0;
                }
                break;
            case RESULT:
                if (action == ACTION_SET_OPERATOR) {
                    mState = CalculatorState.SECOND_NUMBER;
                    break;
                } else if (action == ACTION_INPUT) {
                    mState = CalculatorState.FIRST_NUMBER;
                }
                if (text.length() > 0) {
                    mNumber1 = Long.valueOf(text);
                } else {
                    mNumber1 = 0;
                }
                break;
        }
        Log.d(TAG, String.format("%d [%d] %d\n", mNumber1, mEval, mNumber2));
    }


    /**
     * Called when a view has been clicked.
     *
     * @param v The view that was clicked.
     */
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.equalto:
                onEqualTo();
                break;
            case R.id.del:
                onDelete();
                break;
            case R.id.add:
            case R.id.minus:
            case R.id.times:
                if (mState == CalculatorState.SECOND_NUMBER) {
                    onEqualTo();
                }
                mAction = ACTION_SET_OPERATOR;
                mEval = getAction(v.getId());
                mState = CalculatorState.OPERATOR;
                break;
            default:
                mAction = ACTION_INPUT;
                String text = mResultView.getText().toString();
                if (mState == CalculatorState.RESULT || mState == CalculatorState.OPERATOR) {
                    mResultView.setText(((Button) v).getText());
                } else if (text.length() < MAX_DIGITS){
                    if (text.equals("0")) {
                        mResultView.setText(((Button) v).getText());
                    } else {
                        mResultView.append(((Button) v).getText());
                    }
                }
                break;
        }
    }

    private int getAction(int id) {
        switch (id) {
            case R.id.add:
                mEval = ADD;
                break;
            case R.id.minus:
                mEval = MINUS;
                break;
            case R.id.times:
                mEval = MUL;
                break;
//            default:
//                return ADD;
        }
        return mEval;
    }

    private void onEqualTo() {
        mAction = ACTION_CALCULATE;
        mLastResult = calculate();
        mResultView.setText(String.valueOf(mLastResult));
    }

    private void onDelete() {
        if (mResultView != null) {
            final Editable text = mResultView.getEditableText();
            final int length = text.length();
            if (length > 0) {
                if (mState.equals(CalculatorState.RESULT)) {
                    text.clear();
                } else {
                    text.delete(length - 1, length);
                }
            }
        }
    }


    private int calculate() {
        final int ret = getCalculator(mEval).calculate(mNumber1, mNumber2);
        Log.d(TAG, String.format("%d = %d (%s) %d\n", ret, mNumber1, mEval, mNumber2));
        return ret;
    }

    private ICalculator getCalculatorOperator(int action) {
        switch (action) {
            case ADD:
                return new AddCalculator();
            case MINUS:
                return new MinusCalculator();
            case MUL:
                return new MultiplyCalculator();
        }
        return null;
    }

    public Calculator getCalculator(int action) {
        return new Calculator(getCalculatorOperator(action));
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        Editable text = mResultView.getEditableText();
        if (text != null && text.length() > 0) {
            outState.putLong("RESULT_VALUE", Long.valueOf(text.toString()));
            outState.putInt("EVAL", mEval);
        }

        super.onSaveInstanceState(outState);
    }
}
