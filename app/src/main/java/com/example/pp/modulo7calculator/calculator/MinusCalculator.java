package com.example.pp.modulo7calculator.calculator;

import com.example.pp.modulo7calculator.adapter.ComputingAdapter;

/**
 * Created by pp on 16/3/12.
 */

public class MinusCalculator implements ICalculator {
    @Override
    public int compute(long a, long b) {
        return new ComputingAdapter().compute(a, b, Calculator.MINUS);
    }

    @Override
    public int getOperator() {
        return Calculator.MINUS;
    }
}
