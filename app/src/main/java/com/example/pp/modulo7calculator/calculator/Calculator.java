package com.example.pp.modulo7calculator.calculator;

import android.util.Log;

/**
 * Created by pp on 16/3/12.
 */
public class Calculator {
    private static final String TAG = "Calculator";

    // Define actions need to be performed
    public static final int ADD = 1;
    public static final int MINUS = ADD << 1;
    public static final int MUL = ADD << 2;

    private ICalculator mCalculator;

    private long mFirstNumber, mSecondNumber;
    private int mResult;

    public long getFirstNumber() {
        return mFirstNumber;
    }

    public void setFirstNumber(long number) {
        this.mFirstNumber = number;
    }

    public long getSecondNumber() {
        return mSecondNumber;
    }

    public void setSecondNumber(long number) {
        this.mSecondNumber = number;
    }

    public int getOperator() {
        return mCalculator.getOperator();
    }

    public int getResult() {
        return mResult;
    }


    public Calculator(ICalculator calculator) {
        if (calculator == null) {
            Log.e(TAG, "No invalid calculator");
        }
        this.mCalculator = calculator;
    }

    public int calculate(long a, long b) {
        mResult = mCalculator.compute(a, b);
        return mResult;
    }
}
