package com.example.pp.modulo7calculator.calculator;

/**
 * Created by pp on 16/3/12.
 */
public interface ICalculator {
    int compute(long a, long b);
    int getOperator();
}
