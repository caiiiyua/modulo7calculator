#include "mycalculator.h"
#include <stdio.h>

/**
 * add: x + y
 */
long add(long x, long y) {
    return x + y;
}

/**
 * minus: x - y
 */
long minus(long x, long y) {
    return x - y;
}

/**
 * times: x * y
 */
long times(long x, long y) {
    return x * y;
}

#define ADD 1
#define MINUS ADD << 1
#define MULTIPLE ADD << 2
#define MODULO 7 // Define number for modulo

/**
 * compute: perform calculate with x and y
 * according to cmd
 */
int compute(long x, long y, int cmd) {
    long r = 0;
    switch (cmd) {
        case ADD: {
            r = add(x, y);
            break;
        }
        case MINUS: {
            r = minus(x, y);
            break;
        }
        case MULTIPLE: {
            r = times(x, y);
            break;
        }
        default:
            break;
    }
    printf("result %d : %d", r, r % MODULO);
    return r % MODULO;
}

JNIEXPORT jint JNICALL
Java_com_example_pp_modulo7calculator_adapter_ComputingAdapter_compute(JNIEnv *env,
                                                                       jobject instance, jlong a,
                                                                       jlong b, jint cmd) {
    return (int)compute(a, b, cmd);
}
